import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';

import Punto1 from './Punto1/Punto1';
import Punto2 from './Punto2/Punto2';
import Punto3 from './Punto3/Punto3';
import Punto4 from './Punto4/Punto4';
import Punto5 from './Punto5/Punto5';
import Punto6 from './Punto6/Punto6';
import Punto7 from './Punto7/Punto7';
import Punto8 from './Punto8/Punto8';
import Punto9 from './Punto9/Punto9';
import Punto10 from './Punto10/Punto10';

class App extends Component {

    showTaller(e) {
        console.log(e.target.dataset.punto);
        let punto = Number.parseInt(e.target.dataset.punto);
        if (punto === 1)
            ReactDOM.render( < Punto1 / > , document.getElementById('punto-taller'));
        else if (punto === 2) {
            ReactDOM.render( < Punto2 / > , document.getElementById('punto-taller'));
        } else if (punto === 3) {
            ReactDOM.render( < Punto3 / > , document.getElementById('punto-taller'));
        } else if (punto === 4) {
            ReactDOM.render( < Punto4 / > , document.getElementById('punto-taller'));
        } else if (punto === 5) {
            ReactDOM.render( < Punto5 / > , document.getElementById('punto-taller'));
        } else if (punto === 6) {
            ReactDOM.render( < Punto6 / > , document.getElementById('punto-taller'));
        } else if (punto === 7) {
            ReactDOM.render( < Punto7 / > , document.getElementById('punto-taller'));
        } else if (punto === 8) {
            ReactDOM.render( < Punto8 / > , document.getElementById('punto-taller'));
        } else if (punto === 9) {
            ReactDOM.render( < Punto9 / > , document.getElementById('punto-taller'));
        } else if (punto === 10) {
            ReactDOM.render( < Punto10 / > , document.getElementById('punto-taller'));
        }
    }

    render() {
        const point = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Taller <code>React</code>.
                    </p>
                    <div className="puntos-taller">
                        {point.map((value, index) => {
                            return <a data-punto={value} onClick={this.showTaller}>Punto {value}</a>
                        })}
                    </div>
                </header>
                <div id='punto-taller' className="container"></div>
            </div>
        );
    }
}

export default App;