import React, { Component } from 'react';

class Punto1 extends Component {
    constructor(props) {
        super(props);
        this.state = {numero: '', verificado: ''};

        this.handleChange = this.handleChange.bind(this);
        this.positivoNegativo = this.positivoNegativo.bind(this);
    }

    handleChange = (evt) => {
        this.setState({
            numero: evt.target.value
        });
    }

    positivoNegativo(evt) {
        evt.preventDefault();

        if (!isNaN(Number.parseInt(this.state.numero))) {
            let numValidate = Number.parseInt(this.state.numero);

            if (numValidate > 0)
                this.setState({verificado: 'Número positivo'});
            else if(numValidate < 0)
                this.setState({verificado: 'Numero negativo'});
            else if (numValidate === 0)
                this.setState({verificado: 'Número igual a 0'})
        } else {
            this.setState({numero: '', verificado: 'Por favor ingrese un valor numérico'});
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.positivoNegativo}>
                    <label>
                        Número:
                        <input 
                            type="text" 
                            value={this.state.numero}
                            onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Verificar"></input>
                </form>
                <span>{this.state.verificado}</span>
            </div>
        );
    }
}

export default Punto1;