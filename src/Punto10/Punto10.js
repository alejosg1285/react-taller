import React, { Component } from 'react';

class Punto10 extends Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (evt) => {
        this.setState({
            antiguedad: evt.target.value
        });
    }

    handleSubmit = (evt) => {
        evt.preventDefault();

        let meses = Number.parseInt(this.refs.antiguedad.value) / 12;
        let income = 1200000;
        let portion = 0;
        let totalIncome  = 0;

        if (meses < 1) {
            portion = income * 0.05;
        } else if (meses >= 1 && meses < 2) {
            portion = income * 0.07;
        } else if (meses >= 2 && meses < 5) {
            portion = income * 0.10;
        } else if (meses >= 5 && meses < 10) {
            portion = income * 0.15;
        } else if (meses >= 10) {
            portion = income * 0.20;
        }
        
        totalIncome = portion + income;

        this.setState({
            utilidadAntiguedad: `Salario: $${income}, utilidad: $${portion}, total: $${totalIncome}`
        });
    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Antiguedad (meses): 
                        <input
                            type="number"
                            name="antiguedad"
                            ref="antiguedad" />
                    </label>
                    <input type="submit" value="Calcular utilidad" />
                </form>
                <label>{this.state.utilidadAntiguedad}</label>
            </div>
        );
    }
}

export default Punto10;