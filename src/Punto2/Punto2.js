import React, { Component } from 'react';

class Punto2 extends Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.handleChange = this.handleChange.bind(this);
        this.handleCalc = this.handleCalc.bind(this);
    }

    handleChange = (evt) => {
        this.setState({
            numero: evt.target.value
        });
    }

    handleCalc = (evt) => {
        evt.preventDefault();

        let num = Number.parseInt(this.state.numero);

        let res = num % 2;

        if (res === 0) {
            this.setState({
                resModule: `El numero ${num} es par`
            });
        } else {
            this.setState({
                resModule: `El número ${num} es impar`
            });
        }
    }

    render() {
        return (
            <div className="container">
                <form>
                    <label>
                        Ingrese un número: 
                        <input 
                            type="number"
                            name="numero"
                            value={this.state.numero}
                            onChange={this.handleChange} />
                    </label>
                    <button type="button" onClick={this.handleCalc}>Calcular residuo</button>
                </form>
                <label>{this.state.resModule}</label>
            </div>
        );
    }
}

export default Punto2;