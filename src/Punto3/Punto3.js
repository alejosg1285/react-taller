import React from 'react';
import TextInput from '../Elements/TextInput';
import validate from '../validations/validate';

class Punto3 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sizedig: {
                numDigit: '',
                placeholder: 'Digite un número',
                valid: 'false',
                touched: 'false',
                validationRules: {
                    maxLenght: 5,
                    isRequired: true
                }
            },
            lengthDigit: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (evt) => {
        const value = evt.target.value;
        const name = evt.target.name;
        
        const updatedControls = {
            ...this.state.sizedig
        };
        
        const updatedFormElement = {
            ...updatedControls[name]
        };
        
        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        updatedControls[name] = updatedFormElement;

        this.setState({
            [name]: updatedControls
            /*[name]: {
                numDigit: value
            }*/
        });
    }

    handleSubmit = (evt) => {
        evt.preventDefault();

        //console.log(this.state.sizedig);
        let digit = this.state.sizedig.sizedig.numDigit;
        
        let lnDigit = digit.length;

        if (lnDigit <= 3) {
            this.setState({
                lengthDigit: `El número ${digit} tiene ${lnDigit} dígitos`
            });
        } else {
            this.setState({
                lengthDigit: `El número ${digit} tiene mas de 3 dígitos`
            });
        }
    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Ingrese un número: 
                        <TextInput
                            name="numDigit"
                            placeholder={this.state.sizedig.placeholder}
                            value={this.state.sizedig.value}
                            touched={this.state.sizedig.touched}
                            valid={this.state.sizedig.valid}
                            onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Contar" />
                </form>
                <label>{this.state.lengthDigit}</label>
            </div>
        );
    }
}

export default Punto3;