import React, { Component } from 'react';

class Punto4 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            result: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.compareNumbers = this.compareNumbers.bind(this);
    }

    handleChange = (evt) => {
        let field = evt.target.name;

        this.setState({
            [field]: evt.target.value
        });
    }

    compareNumbers = (evt) => {
        evt.preventDefault();

        let firstNumber = Number.parseInt(this.state.number1);
        let secondNumber = Number.parseInt(this.state.number2);
        let resul = '';

        if (firstNumber === secondNumber)
            resul = `Los números ${firstNumber} y ${secondNumber} son iguales`;
        else
            resul = `Los números ${firstNumber} y ${secondNumber} son diferentes`;

        this.setState({
            result: resul
        });
    }

    render() {
        return (
            <div className="container">
                <form>
                    <p>
                        <label>
                            Numero 1: 
                            <input
                                type="number"
                                name="number1"
                                value={this.state.number1}
                                onChange={this.handleChange} />
                        </label>
                    </p>
                    <p>
                        <label>
                            Número 2: 
                            <input
                                type="number"
                                name="number2"
                                value={this.state.number2}
                                onChange={this.handleChange} />
                        </label>
                    </p>
                    <button type="button" onClick={this.compareNumbers}>Comparar</button>
                </form>
                <label>{this.state.result}</label>
            </div>
        );
    }
}

export default Punto4;