import React, { Component } from 'react';

class Punto5 extends Component {
    constructor() {
        super();

        this.state = {
            num1: 2,
            num2: 0,
            num3: 0,
            mayor: 'Digite 3 digitos'
        };

        this.buscarMayor = this.buscarMayor.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (e) => {
        //console.log(e.target);
        let nameField = e.target.name;
        this.setState({
            [nameField]: e.target.value
        });
    }

    buscarMayor = (evt) => {
        evt.preventDefault();
        
        let num1 = Number.parseInt(this.state.num1);
        let num2 = Number.parseInt(this.state.num2);
        let num3 = Number.parseInt(this.state.num3);
        
        if (num1 > num2 && num1 > num3)
            this.setState({mayor: 'El mayor es '+num1});
        else if (num2 > num1 && num2 > num3)
            this.setState({mayor: 'El mayor es '+num2});
        else if (num3 > num1 && num3 > num2)
            this.setState({mayor: 'El mayor es '+num3});
    }

    render() {
        return (
            <div className="container">
                <form submit={this.handleSubmit}>
                    <label>
                        Número 1 
                        <input 
                            type="number"
                            name="num1" 
                            value={this.state.num1}
                            onChange={this.handleChange} />
                    </label>
                    <label>
                        Número 2 <input 
                            type="number" 
                            name="num2" 
                            value={this.state.num2} 
                            onChange={this.handleChange} />
                    </label>
                    <label>
                        Número 3
                        <input 
                            type="number" 
                            name="num3" 
                            value={this.state.num3}
                            onChange={this.handleChange} />
                    </label>
                    <button type="button" onClick={this.buscarMayor}>Verificar mayor</button>
                </form>
                <label>{this.state.mayor}</label>
            </div>
        );
    }
}

export default Punto5;