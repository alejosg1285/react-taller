import React, { Component } from 'react';

class Punto6 extends Component {
    constructor() {
        super();

        this.state = {
            score1: 1,
            score2: 1,
            score3: 1,
            mayor: 'Digite 3 digitos'
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (e) => {
        //console.log(e.target);
        let nameField = e.target.name;
        this.setState({
            [nameField]: e.target.value
        });
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        
        let score1 = Number.parseInt(this.state.score1);
        let score2 = Number.parseInt(this.state.score2);
        let score3 = Number.parseInt(this.state.score3);
        
        let finalScore = (score1 + score2 + score3) / 3;
        if (finalScore >= 3.5)
            this.setState({resultScore: 'El alumno alcanzo a pasar'});
        else
            this.setState({resultScore: 'Le toco repetir'});
    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Nota pacrial 1 
                        <input 
                            type="number"
                            name="score1"
                            min={1} 
                            max={5}
                            value={this.state.score1}
                            onChange={this.handleChange} />
                    </label>
                    <label>
                        Nota pacrial 2 
                        <input 
                            type="number" 
                            name="score2" 
                            max="5" 
                            value={this.state.score2} 
                            onChange={this.handleChange} />
                    </label>
                    <label>
                        Nota pacrial 3
                        <input 
                            type="number" 
                            name="score3" 
                            max="5" 
                            value={this.state.score3}
                            onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Validar notas"/>
                </form>
                <label>{this.state.resultScore}</label>
            </div>
        );
    }
}

export default Punto6;