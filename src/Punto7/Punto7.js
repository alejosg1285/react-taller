import React from 'react';

class Punto7 extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			purchase: 0
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange = (evt) => {
		evt.preventDefault();

		this.setState({
			purchase: evt.target.value
		});
	}

	handleSubmit = (evt) => {
		evt.preventDefault();

		let value = this.state.purchase;

		let porcValue = 0;

		if (value >= 5000) {
			porcValue = value * 0.20;
		}

		let totalValue = value - porcValue;
		
		this.setState({
			buyTotal: `Total a pagar: $${totalValue}`
		});
	}

	render() {
		return (
			<div className="container">
				<form onSubmit={this.handleSubmit}>
					<label>
						Valor ($):
						<input
							type="number"
							name="purchase"
							value={this.state.purchase}
							onChange={this.handleChange} />
					</label>
					<input type="submit" value="Pagar" />
				</form>
				<label>{this.state.buyTotal}</label>
			</div>
			);
	}
}

export default Punto7;