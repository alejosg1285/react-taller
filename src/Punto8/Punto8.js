import React, { Component } from 'react';

class Punto8 extends Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.handleChange = this.handleChange.bind(this);
		this.calcPay = this.calcPay.bind(this);
	}

	handleChange = (evt) => {
		this.setState({
			weekHours: evt.target.value
		});
	}

	calcPay = (evt) => {
		evt.preventDefault();

		let hours = this.state.weekHours;

		let valueHour = 0;
		let extraHour = 0;

		if (hours > 40) {
			valueHour = 40 * 2000;
			extraHour = (hours - 40) * 3000;
		} else {
			valueHour = hours * 2000;
		}
		let total = valueHour + extraHour;
		this.setState({
			income: `Salario total: $${total}`
		});
	}

	render() {
		return (
			<div className="container">
				<form>
					<label>
						Horas semanales: 
						<input
							type="number"
							name="weekHours"
							value={this.state.hours}
							onChange={this.handleChange} />
					</label>
					<input type="button" onClick={this.calcPay} value="Calcular" />
				</form>
				<label>{this.state.income}</label>
			</div>
			);
	}
}

export default Punto8;