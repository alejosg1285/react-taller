import React from 'react';

class Punto9 extends React.Component {
	constructor(props) {
		super(props);

		this.state = {};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange = (evt) => {
		this.setState({
			cant_camisas: evt.target.value
		});
	}


	handleSubmit = (evt) => {
		evt.preventDefault();

		let cantCamisas = Number.parseInt(this.state.cant_camisas);
		let discount = 0;
		let valorCamisa = 18000;

		if (cantCamisas <= 3) {
			discount = valorCamisa * 0.10;
		} else if (cantCamisas === 4) {
			discount = valorCamisa * 0.20;
		} else if (cantCamisas >= 5) {
			discount = valorCamisa * 0.40;
		}

		let totalPay = (cantCamisas * valorCamisa) - discount;

		this.setState({
			pagar: `Cantidad articulos ${cantCamisas} \n Total a pagar $${totalPay} \n Total descuento: $${discount}`
		});
	}

	render() {
		return (
			<div className="container">
				<form onSubmit={this.handleSubmit}>
					<label>
						Total camisas: 
						<input
							type="number"
							name="cant_camisas"
							value={this.state.camisas}
							onChange={this.handleChange} />
					</label>
					<input type="submit" value="Pagar" />
					<label>{this.state.pagar}</label>
				</form>
			</div>
		);
	}
}

export default Punto9;